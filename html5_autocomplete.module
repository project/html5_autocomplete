<?php

/**
 * @file
 * Allows HTML5 autocomplete attribute to be set on form input elements.
 */

/**
 * Implements hook_form_builder_load().
 */
function html5_autocomplete_form_builder_load_alter(&$form, $form_type, $form_id) {
  if ($form_type == 'webform') {
    foreach (element_children($form) as $cid) {
      if (isset($form[$cid]['#attributes']['autocomplete'])) {
        $form[$cid]['#autocomplete'] = $form[$cid]['#attributes']['autocomplete'];
      }
      if (isset($form[$cid]['#attributes']['vcard_name'])) {
        $form[$cid]['#vcard_name'] = $form[$cid]['#attributes']['vcard_name'];
      }
    }
  }
}

/**
 * Implements hook_node_presave().
 */
function html5_autocomplete_node_presave($node) {
  if ($node->type == 'webform') {
    foreach (element_children($node->webform['components']) as $cid) {
      if (empty($node->webform['components'][$cid]['extra']['attributes']['autocomplete'])) {
        unset($node->webform['components'][$cid]['extra']['attributes']['autocomplete']);
      }
      if (empty($node->webform['components'][$cid]['extra']['attributes']['vcard_name'])) {
        unset($node->webform['components'][$cid]['extra']['attributes']['vcard_name']);
      }
    }
  }
}

/**
 * Implements hook_form_builder_properties().
 */
function html5_autocomplete_form_builder_properties($form_type) {
  return array(
    'autocomplete' => array(
      'form' => 'html5_autocomplete_form_builder_property_autocomplete_form',
    ),
    'vcard_name' => array(
      'form' => 'html5_autocomplete_form_builder_property_vard_name_form',
    ),
  );
}

/**
 * Configuration form for the "autocomplete" property.
 */
function html5_autocomplete_form_builder_property_autocomplete_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $default = NULL;
  if (isset($element['#autocomplete'])) {
    $default = $element['#autocomplete'];
  }
  elseif (isset($element['#webform_component']['extra']['attributes']['autocomplete'])) {
    $default = $element['#webform_component']['extra']['attributes']['autocomplete'];
  }

  $form['autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => t('Autocomplete type'),
    '#default_value' => $default,
    '#weight' => 10,
    '#description' => t('Set the type of data that this field expects for browsers that implement type-specific autofill hints. See !link. Note that Chrome only uses this when there are at least 3 fields on a page using this attribute. Set as <em>off</em> to dissuade browsers from autocompleting this field at all.', array('!link' => l(t('the autocomplete documentation for a list of tokens supported by Chrome'), 'http://wiki.whatwg.org/wiki/Autocompletetype', array('fragment' => 'Experimental_Implementation_in_Chrome', 'attributes' => array('target' => '_new'))))),
  );

  return $form;
}

/**
 * Configuration form for the "vcard_name" property.
 */
function html5_autocomplete_form_builder_property_vard_name_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $default = NULL;
  if (isset($element['#vcard_name'])) {
    $default = $element['#vcard_name'];
  }
  elseif (isset($element['#webform_component']['extra']['attributes']['vcard_name'])) {
    $default = $element['#webform_component']['extra']['attributes']['vcard_name'];
  }

  $form['vcard_name'] = array(
    '#type' => 'textfield',
    '#title' => t('vCard name'),
    '#default_value' => $default,
    '#weight' => 11,
    '#description' => t('Set the type of data that this field expects for browsers that implement type-specific autofill hints using the vcard_name attribute. See !link. Set as <em>off</em> to dissuade browsers from autocompleting this field at all.', array('!link' => l(t('the autocomplete documentation for a list of tokens supported by Internet Explorer'), 'http://msdn.microsoft.com/en-us/library/ms533032.aspx', array('fragment' => 'implement', 'attributes' => array('target' => '_new'))))),
  );

  return $form;
}

/**
 * Implements hook_form_builder_webform_property_map_alter().
 */
function html5_autocomplete_form_builder_webform_property_map_alter(&$map, $component_type) {
  if (in_array($component_type, array('textfield', 'email', 'number'), TRUE)) {
    $map['properties']['autocomplete'] = array(
      'storage_parents' => array('extra', 'attributes', 'autocomplete'),
    );
    $map['properties']['vcard_name'] = array(
      'storage_parents' => array('extra', 'attributes', 'vcard_name'),
    );
  }
}
